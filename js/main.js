const markerArea = document.querySelector('.marker-area'),
      markerBtnFullInk = document.querySelector('.marker-full'),
      markerBtnItemInk = document.querySelector('.marker-one-step'),
      wrightArea = document.querySelector('.wright-area');

const createMarkerItem = () => {

    const amount = markerArea.children.length;

    if(amount > 9) {
        return false;
    }

    const item = document.createElement('div');
    item.classList.add('marker-item');
    markerArea.appendChild(item);
};

const marker = {

    oneStep() {
        createMarkerItem();
    },

    fullMarker() {
        for(let i = 1; i <= 10; i++) {
            if(i <= 10) {
                createMarkerItem();
            }
        }
    }

}

const oneItemInk = () => {
    marker.oneStep();
    wrightArea.removeAttribute('disabled');
};

const fullInk = () => {
    marker.fullMarker();
    wrightArea.removeAttribute('disabled');
};

const counterInput = () => {

    let reg = /^\S*$/;

    if(wrightArea.value.length % 5 === 0 && reg.test(wrightArea.value) === true) {

        let children = Array.from(markerArea.children),
            lastItem = children[children.length - 1];
            markerArea.removeChild(lastItem);
            children.pop();

        if(children.length === 0) {
            wrightArea.setAttribute('disabled', '');

            if(wrightArea.hasAttribute('disabled')) {
               setTimeout(() => {
                    alert('У вас закончились чернила, пополните их запас!');
               }, 500); 
            }
        }
    }
};

markerBtnFullInk.addEventListener('click', fullInk);
markerBtnItemInk.addEventListener('click', oneItemInk);
wrightArea.addEventListener('input', counterInput);

marker.fullMarker();